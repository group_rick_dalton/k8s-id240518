FROM python:3.8-alpine as builder

RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

WORKDIR /opt

COPY . /opt
RUN pip install -r requirements.txt && pip install -r requirements-server.txt && \
    flask db upgrade && python seed.py

FROM python:3.8-alpine
COPY --from=builder /opt /opt
WORKDIR /opt
ENV PATH="/opt/venv/bin:$PATH"

ENTRYPOINT ["gunicorn", "app:app"]
CMD ["-b", "0.0.0.0:8000"]
